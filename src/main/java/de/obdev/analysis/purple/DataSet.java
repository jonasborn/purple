package de.obdev.analysis.purple;

import org.neuroph.core.data.DataSetRow;

import java.util.ArrayList;
import java.util.List;

public class DataSet {

    transient List<ArrayList<Double>> sets = new ArrayList<ArrayList<Double>>();
    Integer inputSize;
    Integer outputSize;

    double min = Double.MAX_VALUE;
    double max = 0;

    org.neuroph.core.data.DataSet dataSet;

    public DataSet(Integer inputSize, Integer outputSize) {
        this.inputSize = inputSize;
        this.outputSize = outputSize;
    }

    public DataSet(DataSet dataSet) {
        min = dataSet.min;
        max = dataSet.max;
        inputSize = dataSet.inputSize;
        outputSize = dataSet.outputSize;
    }

    public org.neuroph.core.data.DataSet get() {
        for (ArrayList<Double> l : sets) {
            for (Double l3 : l) {
                if (l3 > max) max = l3;
                if (l3 < min) min = l3;
            }
        }
        this.dataSet = new org.neuroph.core.data.DataSet(inputSize, outputSize);
        for (ArrayList<Double> l : sets) {
            for (ArrayList<Double> ent : prepareSingle(l)) {

                ArrayList<Double> out = new ArrayList<>();

                for (int i = 0; i < outputSize; i++) {
                    out.add(ent.remove(ent.size() - 1));
                }

                DataSetRow row = new DataSetRow(
                        ent, out
                );
                dataSet.addRow(row);
            }
        }
        return dataSet;
    }

    public List<ArrayList<Double>> prepareSingle(List<Double> list) {
        List<ArrayList<Double>> sets = new ArrayList<>();
        Integer pos = 0;
        for (Double l : list) {
            ArrayList<Double> set = new ArrayList<>();
            if (pos + inputSize + outputSize <= list.size()) {
                set.add(normalizeValue(l));
                for (int i = 1; i < inputSize + outputSize; i++) {
                    set.add(normalizeValue(list.get(pos + i)));
                }
                sets.add(set);
            }
            pos++;
        }
        return sets;
    }


    public List<Double> getTestData() {
        List<Double> values = new ArrayList<>();
        for (int i = 0; i < sets.size(); i++) {
            if (values.size() > inputSize) break;
            values.addAll(prepareSingle(sets.get(i)).get(0));
        }
        return values;
    }

    public void addSet(List<Double> set) {
        if (!sets.contains(set)) {
            sets.add(new ArrayList<>(set));
        }
    }

    public double normalizeValue(double input) {
        return (input - min) / (max - min) * 0.8 + 0.1;
    }

    public double deNormalizeValue(double input) {
        return min + (input - 0.1) * (max - min) / 0.8;
    }


}
