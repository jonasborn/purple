package de.obdev.analysis.purple;

/**
 * Created by obdev on 26.01.2017.
 */
public abstract class PurpleListener {

    Integer multiplier = 10;

    public PurpleListener() {
    }

    public PurpleListener(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public abstract void onProgress(Integer total, Integer current, Integer percent);

    public void onFinish() {

    }

}
