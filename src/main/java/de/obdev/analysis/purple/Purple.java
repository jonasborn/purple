package de.obdev.analysis.purple;

import com.google.common.collect.Lists;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.core.exceptions.NeurophException;
import org.neuroph.core.learning.SupervisedLearning;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by obdev on 25.01.2017.
 */
public class Purple implements Serializable {

     Integer maxIterations = 10000000;
     Double learningRate = 0.5;
     Double maxError = 0.00001;

    double min = Double.MAX_VALUE;
    double max = 0;

    Double totalError = Double.MAX_VALUE;

    NeuralNetwork<BackPropagation> neuralNetwork;
    Integer inputSize;
    Integer outputSize;

    transient List<PurpleListener> listeners = new ArrayList<>();


    transient DataSet dataSet;

    public Purple setMaxIterations(Integer maxIterations) {
        this.maxIterations = maxIterations;
        return this;
    }

    public Purple setLearningRate(Double learningRate) {
        this.learningRate = learningRate;
        return this;
    }

    public Purple setMaxError(Double maxError) {
        this.maxError = maxError;
        return this;
    }

    public Purple(Integer inputSize, Integer outputSize) {
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        neuralNetwork = new MultiLayerPerceptron(
                inputSize,
                2 * inputSize + 1,
                outputSize
        );

    }

    public Purple(Integer inputSize, Integer outputSize, Integer hiddenSize) {
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        neuralNetwork = new MultiLayerPerceptron(
                inputSize,
                hiddenSize,
                outputSize
        );
    }

    public static Purple get(byte[] input) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(input));
        return (Purple) inputStream.readObject();
    }

    public static Purple get(File input) throws IOException, ClassNotFoundException {
        FileInputStream fin = new FileInputStream(input);
        ObjectInputStream inputStream = new ObjectInputStream(fin);
        return (Purple) inputStream.readObject();
    }


    public void addListener(PurpleListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }



    public void prepare() throws IOException {
        this.prepare(maxIterations, learningRate, maxError);
    }

    public void prepare(final Integer maxIterations, Double learningRate, Double maxError) throws IOException {
        SupervisedLearning learningRule = neuralNetwork.getLearningRule();
        learningRule.setMaxError(maxError);
        learningRule.setLearningRate(learningRate);
        learningRule.setMaxIterations(maxIterations);
        learningRule.addListener(new LearningEventListener() {
            @Override
            public void handleLearningEvent(LearningEvent learningEvent) {
                SupervisedLearning rule = (SupervisedLearning) learningEvent.getSource();
                totalError = rule.getTotalNetworkError();
                for (PurpleListener purpleListener : listeners) {
                    Integer current = rule.getCurrentIteration();
                    if (rule.getCurrentIteration() % purpleListener.getMultiplier() == 0) {
                        purpleListener.onProgress(maxIterations, current, (100 * current) / maxIterations);
                    }
                }
            }
        });
        for (PurpleListener purpleListener : listeners) {
            purpleListener.onFinish();
        }
    }

    public Double getTotalError() {
        return totalError;
    }

    public void train(DataSet dataSet) {
        neuralNetwork.learn(dataSet.get());
    }

    public List<Double> predictSingle(DataSet dataSet, List<Double> values) {
        double[] data = new double[values.size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = dataSet.normalizeValue(values.get(i));
        }

        neuralNetwork.setInput(data);
        neuralNetwork.calculate();

        List<Double> output = new ArrayList<>();
        for (double d : neuralNetwork.getOutput()) {
            output.add(dataSet.deNormalizeValue(d));
        }
        return output;
    }

    public List<Double> predict(DataSet dataSet, List<Double> values, Integer amount) {
        List<Double> results = new ArrayList<>();
        List<Double> full = new ArrayList<>(values);
        while (amount > 0) {
            full = Lists.reverse(full);
            List<Double> currentInput = new ArrayList<>();
            for (int i = 0; i < inputSize; i++) {
                currentInput.add(full.get(i));
            }
            full = Lists.reverse(full);
            List<Double> predicted = predictSingle(dataSet, currentInput);
            full.addAll(predicted);
            results.addAll(predicted);
            amount--;
        }

        return results;
    }





    public byte[] get() throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bout);
        objectOutputStream.writeObject(this);
        bout.flush();
        return bout.toByteArray();
    }

    public void save(File file) {
        ObjectOutputStream out = null;

        try {
            out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));

            out.writeObject(this);
            out.flush();
        } catch (IOException var11) {
            throw new NeurophException("Could not write neural network to file!", var11);
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException var10) {

                }
            }

        }
    }

}
