package de.obdev.analysis.purple;

/**
 * Created by obdev on 26.01.2017.
 */
public class TestResult {

    double expected;
    double predicted;
    double difference;

    public TestResult(double expected, double predicted) {
        this.expected = expected;
        this.predicted = predicted;
        difference = expected - predicted;
    }

    @Override
    public String toString() {
        return "TestResult{" +
                "expected=" + expected +
                ", predicted=" + predicted +
                ", difference=" + difference +
                '}';
    }
}
