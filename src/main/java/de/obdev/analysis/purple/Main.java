package de.obdev.analysis.purple;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by obdev on 26.01.2017.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        DataSet set = new DataSet(2, 1);
        set.addSet(Arrays.asList(0.0, 0.0, 0.0));
        set.addSet(Arrays.asList(1.0, 0.0, 1.0));
        set.addSet(Arrays.asList(0.0, 1.0, 1.0));
        set.addSet(Arrays.asList(1.0, 1.0, 1.0));

        Purple purple = new Purple(2, 1);
        purple.setMaxIterations(10);
        purple.prepare();


    }

}
